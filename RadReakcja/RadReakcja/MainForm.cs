﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RadReakcja
{
    public partial class MainForm : Form
    {
        TimeSpan time;

        public MainForm()
        {
            InitializeComponent();

        }

        private async void button1_Click(object sender, EventArgs e)
        {
            ReactionForm reactionWindow = new ReactionForm(this);
            reactionWindow.ShowDialog();
        }

        public void passedReaction(TimeSpan t)
        {
            time = t;
            labelResult.Text = String.Format("{0:00}:{1:00}:{2:00}", time.Minutes, time.Seconds, time.Milliseconds);
        }
    }
}
