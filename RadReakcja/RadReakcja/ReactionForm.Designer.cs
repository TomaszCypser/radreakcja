﻿namespace RadReakcja
{
    partial class ReactionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonReaction = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.buttonReaction.BackColor = System.Drawing.Color.Red;
            this.buttonReaction.Location = new System.Drawing.Point(13, 70);
            this.buttonReaction.Name = "button1";
            this.buttonReaction.Size = new System.Drawing.Size(613, 111);
            this.buttonReaction.TabIndex = 0;
            this.buttonReaction.Text = "Nie naciskaj";
            this.buttonReaction.UseVisualStyleBackColor = false;
            this.buttonReaction.Click += new System.EventHandler(this.buttonReaction_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(638, 264);
            this.Controls.Add(this.buttonReaction);
            this.Name = "Form2";
            this.Text = "Czekaj na zielony..";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonReaction;
    }
}