﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RadReakcja
{
    public partial class ReactionForm : Form
    {
        MainForm parent;
        Stopwatch time;

        public ReactionForm(MainForm p)
        {
            InitializeComponent();
            parent = p;
            test();
        }
        public async void  test()
        {
            Random rnd = new Random();
            buttonReaction.Enabled = false;
            await Task.Delay(rnd.Next(2000, 15000));
            buttonReaction.Enabled = true;
            buttonReaction.BackColor = Color.Green;
            buttonReaction.Text = "NACIŚNIJ!";
            time = Stopwatch.StartNew();
        }

        private void buttonReaction_Click(object sender, EventArgs e)
        {
            time.Stop();
            TimeSpan timespan = time.Elapsed;
            parent.passedReaction(timespan);
            this.Close();
        }
    }
}
